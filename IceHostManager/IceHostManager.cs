using System;
using CAALHP.Library.Brokers;
using CAALHP.Library.Hosts;
using CAALHP.Library.Managers;
using CAALHP.Library.Visitor;
using CAALHP.SOA.ICE.Host;

namespace IceHostManager
{
    public class IceHostManager : IHostManager, IDisposable
    {
        public IAppHost AppHost { get; private set; }
        public IDeviceDriverHost DeviceDriverHost { get; private set; }
        public IServiceHost ServiceHost { get; private set; }

        public IceHostManager(EventManager eventManager, ICAALHPBroker broker)
        {
            //start the servicehost first so we have loggingservice ready.
            ServiceHost = new ServiceHost(eventManager, broker);
            DeviceDriverHost = new DeviceDriverHost(eventManager, broker);
            AppHost = new AppHost(eventManager, broker);
            StartSystemApps();
        }

        private void StartSystemApps()
        {
            var apps = AppHost.GetLocalInstalledApps();
            for (var i = 0; i < apps.Count; i++)
            {
                if (apps[i].Config.RunAtStartup)
                {
                    AppHost.ShowLocalApp(apps[i].Config.PluginName);
                }
            }
        }

        public void Dispose()
        {
            AppHost.ShutDown();
            DeviceDriverHost.ShutDown();
            ServiceHost.ShutDown();
        }

        public void Accept(IHostVisitor visitor)
        {
            visitor.Visit(this);
            AppHost.Accept(visitor);
            DeviceDriverHost.Accept(visitor);
            ServiceHost.Accept(visitor);
        }
    }
}