﻿using CAALHP.Events;
using CAALHP.Library.Config;
using CAALHP.SOAICE.Contracts;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using System.Collections.Generic;
using System.Diagnostics;

namespace CAALHP.SOA.ICE.Host.Processes
{
    public class ServiceProcess : BaseProcess
    {
        public IServiceContractPrx Service { get; set; }
        public ServiceProcess(Process process, PluginConfig config) : base(process, config) { }
        public override void Update(KeyValuePair<string, string> theEvent)
        {
            if (Service != null) Service.Notify(theEvent.Key, theEvent.Value);
        }

        public override void NewEventTypeUpdate(string newFqns)
        {
            var newEvent = new NewEventTypeAddedEvent
            {
                EventType = newFqns,
                CallerName = GetType().FullName,
                //CallerProcessId = Id
            };
            var value = EventHelper.CreateEvent(SerializationType.Json, newEvent);
            Update(value);
        }
    }
}