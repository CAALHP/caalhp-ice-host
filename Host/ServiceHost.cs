﻿using CAALHP.Library.Brokers;
using CAALHP.Library.Config;
using CAALHP.Library.Managers;
using CAALHP.Library.Visitor;
using CAALHP.SOA.ICE.Host.LifeCycleManagers;
using CAALHP.SOA.ICE.HostAdapters;
using CAALHP.SOAICE.Contracts;
using Ice;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Exception = System.Exception;

namespace CAALHP.SOA.ICE.Host
{
    public class ServiceHost : Host, IServiceHostIce
    {
        private Ice.Communicator _ic;
        private readonly string _serviceRoot;
        private readonly ICAALHPBroker _broker;
        private ObjectAdapter _adapter;

        public ServiceLifeCycleManager LifeCycleManager { get; private set; }

        public ServiceHost(EventManager eventManager, ICAALHPBroker broker)
            : base(eventManager)
        {
            _broker = broker;
            _serviceRoot = Path.Combine(IceRoot, "Services");
            if (!Directory.Exists(_serviceRoot))
            {
                Directory.CreateDirectory(_serviceRoot);
            }
            //Create lifecyclemanager
            LifeCycleManager = new ServiceLifeCycleManager(_serviceRoot);

            //Start ice server
            StartIce();

            //Start services
            ActivateServices();
        }

        private void StartIce()
        {
            //_status = 0;
            _ic = null;
            try
            {
                _ic = Ice.Util.initialize();
                _adapter = _ic.createObjectAdapterWithEndpoints("ServiceHostAdapter", "default -p 10001");
                var obj = new ServiceHostAdapter(this);
                _adapter.add(obj, Ice.Util.stringToIdentity("ServiceHost"));
                _adapter.activate();
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e);
                //_status = 1;
            }

        }

        ~ServiceHost()
        {
            StopServices();
            //stop ice server
            if (_ic != null)
            {
                // Clean up
                //
                try
                {
                    _ic.destroy();
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine(e);
                    //_status = 1;
                }
            }
            //Environment.Exit(_status);
        }

        public void ActivateDrivers()
        {
            _broker.ActivateDeviceDrivers();
        }

        public IList<string> GetListOfEventTypes()
        {
            return EventManager.EventSubject.NameSpaces;
        }

        public void RegisterService(string name, string category, int processId, Current current__)
        {
            lock (this)
            {
                //System.Console.Out.WriteLine("adding client `" + _communicator.identityToString(ident) + "'");
                var task = new Task(() =>
                {

                    try
                    {
                        Ice.ObjectPrx @base = current__.con.createProxy(new Identity(name, category));
                        var client = IServiceContractPrxHelper.uncheckedCast(@base);
                        if (LifeCycleManager.ServiceDictionary.ContainsKey(processId))
                            LifeCycleManager.ServiceDictionary[processId].Service = client;
                        Console.WriteLine("added a new app: " + client.GetName());
                        //Console.WriteLine("GetName() = " + client.GetName());
                        //client.Initialize(null, processId);
                        //client.Notify("Test", "Fedt");
                        //client.Stop();
                        //LifeCycleManager.ServiceDictionary[processId].Service.Initialize(HostView, processId);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error during registration of CareStoreService callback " + e);
                    }
                });
                task.Start();
            }
        }

        private void ActivateServices()
        {
            LifeCycleManager.StartPlugins();
        }


        private void StopServices()
        {
            LifeCycleManager.StopPlugins();
        }

        public override IList<PluginConfig> GetListOfInstalledApps()
        {
            return _broker != null ? _broker.GetListOfInstalledApps() : null;
        }

        public override IList<PluginConfig> GetListOfInstalledDeviceDrivers()
        {
            return _broker != null ? _broker.GetListOfInstalledDeviceDrivers() : null;
        }

        public override void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            if (!LifeCycleManager.ServiceDictionary.ContainsKey(processId)) return;
            var plugin = LifeCycleManager.ServiceDictionary[processId];
            EventManager.EventSubject.Detach(plugin, fullyQualifiedNameSpace);
        }

        public override void SubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            if (!LifeCycleManager.ServiceDictionary.ContainsKey(processId)) return;
            var plugin = LifeCycleManager.ServiceDictionary[processId];
            EventManager.EventSubject.Attach(plugin, fullyQualifiedNameSpace);
        }

        public override void ShutDown()
        {
            LifeCycleManager.StopPlugins();
        }

        public override void Accept(IHostVisitor visitor)
        {
            visitor.Visit(this);
        }

        public override void ShowApp(string appName)
        {
            if (_broker == null) return;
            _broker.ShowApp(appName);
        }

        public override void CloseApp(string appName)
        {
            if (_broker != null)
            {
                _broker.CloseApp(appName);
            }
        }
    }
}