using System;
using System.Runtime.InteropServices;

namespace CAALHP.SOA.ICE.Host
{
    public static class User32
    {
        private const int SW_HIDE = 0;
        private const int SW_SHOWNORMAL = 1;
        private const int SW_SHOWMINIMIZED = 2;
        private const int SW_SHOWMAXIMIZED = 3;
        private const int SW_SHOWNOACTIVATE = 4;
        private const int SW_RESTORE = 9;
        private const int SW_SHOWDEFAULT = 10;

        [DllImport("user32.dll")]
        public static extern bool ShowWindowAsync(HandleRef hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        public static void FocusProcess(string procName)
        {
            var objProcesses = System.Diagnostics.Process.GetProcessesByName(procName);
            if (objProcesses.Length <= 0) return;
            var hWnd = objProcesses[0].MainWindowHandle;
            ShowWindowAsync(new HandleRef(null, hWnd), SW_SHOWMAXIMIZED);
            SetForegroundWindow(objProcesses[0].MainWindowHandle);
        }

        public static void FocusProcess(int procId)
        {
            var objProcess = System.Diagnostics.Process.GetProcessById(procId);
            var hWnd = objProcess.MainWindowHandle;
            ShowWindowAsync(new HandleRef(null, hWnd), SW_SHOWMAXIMIZED);
            SetForegroundWindow(objProcess.MainWindowHandle);
        }
    }
}