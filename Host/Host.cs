﻿using CAALHP.Library.Config;
using CAALHP.Library.Hosts;
using CAALHP.Library.Managers;
using CAALHP.Library.Visitor;
using System;
using System.Collections.Generic;
using System.IO;

namespace CAALHP.SOA.ICE.Host
{
    public abstract class Host : IHost
    {
        protected string IceRoot { get; private set; }
        protected EventManager EventManager { get; private set; }

        protected Host(EventManager eventManager)
        {
            EventManager = eventManager;
            // Assume that the current directory is the application folder,  
            // and that it contains the pipeline folder structure.
            IceRoot = Environment.CurrentDirectory + "\\Ice";
            if (!Directory.Exists(IceRoot))
            {
                Directory.CreateDirectory(IceRoot);
            }
        }

        public abstract void SubscribeToEvents(string fullyQualifiedNameSpace, int processId);

        public abstract void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId);

        public void ReportEvent(KeyValuePair<string, string> value)
        {
            //EventManager.EventSubject.SubjectState = value;
            EventManager.EventSubject.Notify(value);
        }

        public abstract IList<PluginConfig> GetListOfInstalledApps();
        public abstract void ShowApp(string appName);
        public abstract void CloseApp(string appName);
        public abstract IList<PluginConfig> GetListOfInstalledDeviceDrivers();
        public abstract void ShutDown();
        public abstract void Accept(IHostVisitor visitor);
    }
}
