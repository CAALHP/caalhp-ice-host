﻿using CAALHP.Library.Brokers;
using CAALHP.Library.Config;
using CAALHP.Library.Hosts;
using CAALHP.Library.Managers;
using CAALHP.Library.Visitor;
using CAALHP.SOA.ICE.Host.LifeCycleManagers;
using CAALHP.SOA.ICE.HostAdapters;
using CAALHP.SOAICE.Contracts;
using Ice;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Exception = System.Exception;

namespace CAALHP.SOA.ICE.Host
{
    public class DeviceDriverHost : Host, IDeviceDriverHostIce
    {
        private readonly IDeviceDriverHostContractDisp_ _iceAdapter;
        private Ice.Communicator _ic;
        private readonly string _deviceDriverRoot;
        private readonly ICAALHPBroker _broker;
        public DeviceDriverLifeCycleManager LifeCycleManager { get; private set; }

        public DeviceDriverHost(EventManager eventManager, ICAALHPBroker broker)
            : base(eventManager)
        {
            _broker = broker;
            _deviceDriverRoot = Path.Combine(IceRoot, "DeviceDrivers");
            if (!Directory.Exists(_deviceDriverRoot))
            {
                Directory.CreateDirectory(_deviceDriverRoot);
            }
            //Create lifecyclemanager
            LifeCycleManager = new DeviceDriverLifeCycleManager(_deviceDriverRoot);

            StartIce();
            ActivateDrivers();

        }

        private void StartIce()
        {
            _ic = null;
            try
            {
                _ic = Ice.Util.initialize();
                var adapter = _ic.createObjectAdapterWithEndpoints("DeviceDriverHostAdapter", "default -p 10002");
                var obj = new DeviceDriverHostAdapter(this);
                adapter.add(obj, Ice.Util.stringToIdentity("DeviceDriverHost"));
                adapter.activate();
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e);
            }
        }

        ~DeviceDriverHost()
        {
            //stop ice server
            if (_ic != null)
            {
                // Clean up
                //
                try
                {
                    _ic.destroy();
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine(e);
                    //_status = 1;
                }
            }
            //Environment.Exit(_status);
        }

        public override IList<PluginConfig> GetListOfInstalledApps()
        {
            return _broker != null ? _broker.GetListOfInstalledApps() : null;
        }

        public override void ShowApp(string appName)
        {
            if (_broker == null) return;
            _broker.ShowApp(appName);
        }

        public override void CloseApp(string appName)
        {
            if (_broker == null) return;
            _broker.CloseApp(appName);
        }

        public override IList<PluginConfig> GetListOfInstalledDeviceDrivers()
        {
            return LifeCycleManager.DeviceDriverDictionary.Values.Select(driverprocess => driverprocess.Config).ToList();
        }

        public override void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            if (!LifeCycleManager.DeviceDriverDictionary.ContainsKey(processId)) return;
            var plugin = LifeCycleManager.DeviceDriverDictionary[processId];
            EventManager.EventSubject.Detach(plugin, fullyQualifiedNameSpace);
        }

        public override void SubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            if (!LifeCycleManager.DeviceDriverDictionary.ContainsKey(processId)) return;
            var plugin = LifeCycleManager.DeviceDriverDictionary[processId];
            EventManager.EventSubject.Attach(plugin, fullyQualifiedNameSpace);
        }
        public override void ShutDown()
        {
            LifeCycleManager.StopPlugins();
        }

        public override void Accept(IHostVisitor visitor)
        {
            visitor.Visit(this);
        }

        public void ActivateDrivers()
        {
            LifeCycleManager.StartPlugins();
        }

        public IList<string> GetListOfEventTypes()
        {
            return EventManager.EventSubject.NameSpaces;
        }

        public void RegisterDeviceDriver(string name, string category, int processId, Current current__)
        {
            lock (this)
            {
                var task = new Task(() =>
                {

                    try
                    {
                        Ice.ObjectPrx @base = current__.con.createProxy(new Identity(name, category));
                        var client = IDeviceDriverContractPrxHelper.uncheckedCast(@base);
                        if (LifeCycleManager.DeviceDriverDictionary.ContainsKey(processId))
                            LifeCycleManager.DeviceDriverDictionary[processId].DeviceDriver = client;
                        Console.WriteLine("added a new device driver: " + client.GetName());
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error during registration of CareStoreDeviceDriver callback " + e);
                    }
                });
                task.Start();
            }
        }

        /*public Dictionary<int, ProcessInfo> GetRunningDriversProcessInfo()
        {
            return LifeCycleManager.GetRunningPluginsProcessInfo();
        }*/

        public List<string> GetListOfActiveDevices()
        {
            return LifeCycleManager.DeviceDriverDictionary.Select(record => record.Key + " : " + record.Value.Config.Config.PluginName).ToList();
        }

        public void UpdateDriverList()
        {
            //this rescans for new drivers
            LifeCycleManager.StartPlugins();
        }

        public IList<PluginConfig> GetLocalInstalledDeviceDrivers()
        {
            return LifeCycleManager.GetInstalledPlugins();
        }
    }
}
