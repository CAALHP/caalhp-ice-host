﻿using CAALHP.Library.Config;
using CAALHP.Library.Hosts;
using CAALHP.SOA.ICE.Host.Processes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace CAALHP.SOA.ICE.Host.LifeCycleManagers
{
    public class DeviceDriverLifeCycleManager : LifeCycleManager
    {
        public Dictionary<int, DriverProcess> DeviceDriverDictionary { get; private set; }

        public DeviceDriverLifeCycleManager(string pluginRoot)
            : base(pluginRoot)
        {
            DeviceDriverDictionary = new Dictionary<int, DriverProcess>();
        }

        public override void StopPlugins()
        {
            KeepAliveTimer.Stop();
            ShouldKeepAlive = false;
            foreach (var deviceDriverProcess in DeviceDriverDictionary.Values.Where(deviceDriverProcess => deviceDriverProcess.DeviceDriver != null))
            {
                //We expect communication to break after this call, so we will not wait for a response.
                deviceDriverProcess.DeviceDriver.begin_ShutDown();
            }
        }

        protected override int GetRestartCounter(int processId)
        {
            return DeviceDriverDictionary.ContainsKey(processId) ? DeviceDriverDictionary[processId].RestartCount : int.MaxValue;
        }

        protected override void IncrementRestartCounter(int processId)
        {
            if (DeviceDriverDictionary.ContainsKey(processId)) DeviceDriverDictionary[processId].RestartCount++;
        }

        protected override void ReactivatePlugin(int key)
        {
            var config = DeviceDriverDictionary[key].Config;
            if (config == null) return;
            var filename = Path.Combine(config.Directory, config.Config.RunFiles.First());
            if (!File.Exists(filename)) return;
            var process = Process.Start(filename);
            if (process == null) return;
            var pluginProcess = new DriverProcess(process, config) { RestartCount = DeviceDriverDictionary[key].RestartCount };
            DeviceDriverDictionary.Add(process.Id, pluginProcess);
            DeviceDriverDictionary.Remove(key);
            Console.WriteLine("reactivated app: " + config.Config.PluginName);
        }

        protected override void DeactivatePlugin(int processId)
        {
            try
            {
                if (!DeviceDriverDictionary.ContainsKey(processId)) return;
                var view = DeviceDriverDictionary[processId].DeviceDriver;
                if (view == null) return;
                DeviceDriverDictionary[processId].DeviceDriver = null;
                view.ShutDown();
            }
            catch (Exception)
            {
                Console.WriteLine("plugin was already closed");
            }
            finally
            {
                if (Process.GetProcesses().Any(pr => pr.Id == processId))
                    Process.GetProcessById(processId).Kill();
            }
        }

        protected override void KeepAliveTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            foreach (var deviceDriverProcess in DeviceDriverDictionary)
            {
                var process = deviceDriverProcess;
                Task.Run(() =>
                {
                    if (process.Value.DeviceDriver == null) return;
                    try
                    {
                        if (!process.Value.DeviceDriver.IsAlive())
                        {
                            RestartPlugin(process.Key);
                        }
                    }
                    catch (Exception)
                    {
                        RestartPlugin(process.Key);
                    }
                });
            }
        }

        public override async Task ActivatePlugin(PluginConfig plugin)
        {
            await Task.Run(() =>
            {
                if (PluginIsActive(plugin)) return;
                //start the first file
                var process = Process.Start(Path.Combine(plugin.Directory, plugin.Config.RunFiles.First()));
                var pluginProcess = new DriverProcess(process, plugin);
                if (process != null)
                {
                    DeviceDriverDictionary.Add(process.Id, pluginProcess);
                    Console.WriteLine("activated device driver: " + plugin.Config.PluginName);
                }

            });
        }

        private bool PluginIsActive(PluginConfig config)
        {
            return DeviceDriverDictionary.Values.Any(driverProcess => driverProcess.Config.Directory.Equals(config.Directory));
        }

        /*public Dictionary<int, ProcessInfo> GetRunningPluginsProcessInfo()
        {
            var collection = new Dictionary<int, ProcessInfo>();
            foreach (var record in DeviceDriverDictionary)
            {
                var counter = record.Value.PerfCounter;
                var processInfo = new ProcessInfo
                {
                    ProcessId = record.Value.ProcessId,
                    Name = record.Value.DeviceDriver.ToString()
                };
                counter.CategoryName = "Process";

                counter.CounterName = "% Processor Time";
                processInfo.PercentProcessorTime = counter.NextValue();

                counter.CounterName = "Private Bytes";
                processInfo.PrivateBytes = (Int64)counter.NextValue();
                collection.Add(record.Key, processInfo);
            }
            return collection;
        }*/
    }
}