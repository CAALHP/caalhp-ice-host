﻿using CAALHP.Library.Config;
using CAALHP.SOA.ICE.Host.Processes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace CAALHP.SOA.ICE.Host.LifeCycleManagers
{
    public class AppLifeCycleManager : LifeCycleManager
    {
        public Dictionary<int, AppProcess> AppDictionary { get; private set; }

        public AppLifeCycleManager(string pluginRoot)
            : base(pluginRoot)
        {
            AppDictionary = new Dictionary<int, AppProcess>();
        }

        protected override void KeepAliveTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            foreach (var appProcess in AppDictionary)
            {
                var process = appProcess;
                Task.Run(() =>
                {
                    if (process.Value.App == null) return;
                    try
                    {
                        if (!process.Value.App.IsAlive())
                        {
                            RestartPlugin(process.Key);
                        }
                    }
                    catch (Exception)
                    {
                        //Console.WriteLine(e);
                        RestartPlugin(process.Key);
                    }
                });
            }
        }


        public override async Task ActivatePlugin(PluginConfig plugin)
        {
            await Task.Run(() =>
            {
                if (PluginIsActive(plugin)) return;
                var filename = Path.Combine(plugin.Directory, plugin.Config.RunFiles.First());
                if (!File.Exists(filename)) return;
                var process = Process.Start(filename);
                var pluginProcess = new AppProcess(process, plugin);
                if (process != null)
                {
                    AppDictionary.Add(process.Id, pluginProcess);
                    Console.WriteLine("activated app: " + plugin.Config.PluginName);
                }
            });
        }

        private bool PluginIsActive(PluginConfig plugin)
        {
            return AppDictionary.Values.Any(appProcess => appProcess.Config.Directory.Equals(plugin.Directory));
        }

        public override void StopPlugins()
        {
            KeepAliveTimer.Stop();
            ShouldKeepAlive = false;
            foreach (var appProcess in AppDictionary.Values.Where(appProcess => appProcess.App != null))
            {
                //We expect communication to break after this call, so we will not wait for a response.
                appProcess.App.begin_ShutDown();
            }
        }

        protected override int GetRestartCounter(int processId)
        {
            return AppDictionary.ContainsKey(processId) ? AppDictionary[processId].RestartCount : int.MaxValue;
        }

        protected override void IncrementRestartCounter(int processId)
        {
            if (AppDictionary.ContainsKey(processId)) AppDictionary[processId].RestartCount++;
        }

        protected override void ReactivatePlugin(int key)
        {
            var config = AppDictionary[key].Config;
            if (config == null) return;
            var filename = Path.Combine(config.Directory, config.Config.RunFiles.First());
            if (!File.Exists(filename)) return;
            var process = Process.Start(filename);
            if (process == null) return;
            var pluginProcess = new AppProcess(process, config) { RestartCount = AppDictionary[key].RestartCount };
            AppDictionary.Add(process.Id, pluginProcess);
            AppDictionary.Remove(key);
            Console.WriteLine("reactivated app: " + config.Config.PluginName);
        }

        protected override void DeactivatePlugin(int processId)
        {
            try
            {
                if (!AppDictionary.ContainsKey(processId)) return;
                var view = AppDictionary[processId].App;
                if (view == null) return;
                AppDictionary[processId].App = null;
                view.ShutDown();
            }
            catch (Exception)
            {
                Console.WriteLine("plugin was already closed");
            }
            finally
            {
                if (Process.GetProcesses().Any(pr => pr.Id == processId))
                    Process.GetProcessById(processId).Kill();
            }
        }
    }
}
