using System.Threading.Tasks;
using CAALHP.Library.Config;
using CAALHP.Library.Managers;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Timers;
using System.Xml;

namespace CAALHP.SOA.ICE.Host.LifeCycleManagers
{
    public abstract class LifeCycleManager : ILifeCycleManager
    {
        protected Timer KeepAliveTimer;
        protected string PluginRoot { get; private set; }
        protected bool ShouldKeepAlive;
        private int _maxRestarts;

        protected LifeCycleManager(string pluginRoot)
        {
            Init();
            PluginRoot = pluginRoot;
            ShouldKeepAlive = true;
            KeepAliveTimer = new Timer(1000) { AutoReset = true };
            KeepAliveTimer.Elapsed += KeepAliveTimerOnElapsed;
            KeepAliveTimer.Start();
        }

        private void Init()
        {
            var max = ConfigurationManager.AppSettings.Get("MaxRestarts");
            if (!int.TryParse(max, out _maxRestarts))
            {
                _maxRestarts = int.MaxValue;
            }
        }

        protected abstract void KeepAliveTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs);

        protected IEnumerable<string> ParseFilesToRun(string file)
        {
            var filesList = new List<string>();
            var doc = new XmlDocument();
            doc.Load(file);
            var nodes = doc.SelectNodes("//CareStorePluginConfig//runfiles//runfile");
            if (nodes == null) return filesList;
            filesList.AddRange(from XmlNode node in nodes select Path.Combine(Path.GetDirectoryName(file), node.InnerText));
            return filesList;
        }

        /*protected void ActivatePlugins(IEnumerable<string> plugins, string dir)
        {
            foreach (var plugin in plugins)
            {
                ActivatePlugin(Path.Combine(dir, plugin));
            }
        }*/

        public abstract Task ActivatePlugin(PluginConfig plugin);

        public abstract void StopPlugins();

        public virtual void StartPlugins()
        {
            var installedPlugins = GetInstalledPlugins();
            ActivatePlugins(installedPlugins);
        }

        protected void ActivatePlugins(IList<PluginConfig> installedPlugins)
        {
            foreach (var plugin in installedPlugins)
            {
                ActivatePlugin(plugin);
            }
        }

        public virtual IList<PluginConfig> GetInstalledPlugins()
        {
            var plugins = new List<PluginConfig>();
            var folders = Directory.EnumerateDirectories(PluginRoot);
            foreach (var folder in folders)
            {
                foreach (var file in Directory.GetFiles(folder))
                {
                    var fileName = Path.GetFileName(file);
                    if (fileName != null && fileName.Equals("CareStorePlugin.xml"))
                    {
                        var config = PluginConfigurationManager.GetConfig(file);
                        var pluginConfig = new PluginConfig {Config = config, Directory = Path.GetDirectoryName(file)};
                        plugins.Add(pluginConfig);
                        /*foreach (var runfile in config.RunFiles)
                        {
                            var plugin = new PluginInfo();
                            plugin.LocationDir = Path.GetDirectoryName(file);
                            plugin.Name = runfile;
                            plugins.Add(plugin);
                        }*/
                    }
                }
            }
            return plugins;
        }

        protected void RestartPlugin(int processId)
        {
            if (!ShouldKeepAlive) return;
            //deactivate plugin
            DeactivatePlugin(processId);
            //increase restart counter
            IncrementRestartCounter(processId);

            if (GetRestartCounter(processId) < _maxRestarts) //magic number
                //activate plugin
                ReactivatePlugin(processId);
        }

        protected abstract int GetRestartCounter(int processId);
        protected abstract void IncrementRestartCounter(int processId);
        protected abstract void ReactivatePlugin(int key);
        protected abstract void DeactivatePlugin(int processId);
    }
}