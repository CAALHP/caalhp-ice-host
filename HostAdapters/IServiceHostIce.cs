﻿using CAALHP.Library.Hosts;
using Ice;

namespace CAALHP.SOA.ICE.HostAdapters
{
    public interface IServiceHostIce : IServiceHost
    {
        void RegisterService(string name, string category, int processId, Current current__);
    }
}