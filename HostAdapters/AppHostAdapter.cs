﻿using CAALHP.SOAICE.Contracts;
using Ice;
using System.Collections.Generic;
using System.Linq;
using IPluginInfo = CAALHP.SOAICE.Contracts.IPluginInfo;

namespace CAALHP.SOA.ICE.HostAdapters
{
    public class AppHostAdapter : IAppHostContractDisp_
    {
        private readonly IAppHostIce _appHost;

        public AppHostAdapter(IAppHostIce host)
        {
            _appHost = host;
        }

        public override void ShowApp(string appName, Current current__)
        {
            _appHost.ShowApp(appName);
        }

        public override void CloseApp(string appName, Current current__)
        {
            _appHost.CloseApp(appName);
        }

        public override List<IPluginInfo> GetListOfInstalledApps(Current current__)
        {
            return _appHost.GetListOfInstalledApps().Select(app => new IPluginInfo(app.Config.PluginName, app.Directory)).ToList();
        }

        public override void Register(string name, string category, int processId, Current current__)
        {
            _appHost.RegisterApp(name, category, processId, current__);
        }

        public override void ReportEvent(string key, string value, Current current__)
        {
            _appHost.ReportEvent(new KeyValuePair<string, string>(key, value));
        }

        public override void SubscribeToEvents(string fullyQualifiedNameSpace, int processId, Current current__)
        {
            _appHost.SubscribeToEvents(fullyQualifiedNameSpace, processId);
        }

        public override void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId, Current current__)
        {
            _appHost.UnSubscribeToEvents(fullyQualifiedNameSpace, processId);
        }
    }
}
