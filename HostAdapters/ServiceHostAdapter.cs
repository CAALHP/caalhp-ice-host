﻿using CAALHP.SOAICE.Contracts;
using Ice;
using System.Collections.Generic;
using System.Linq;

namespace CAALHP.SOA.ICE.HostAdapters
{
    public class ServiceHostAdapter : IServiceHostContractDisp_
    {
        //private IHostContract _host;
        private readonly IServiceHostIce _serviceHost;

        public ServiceHostAdapter(IServiceHostIce host)
        {
            _serviceHost = host;
            //_host = new HostAdapter(_serviceHost);
        }

        public override List<IPluginInfo> GetListOfInstalledApps(Current current__)
        {
            return _serviceHost.GetListOfInstalledApps().Select(app => new IPluginInfo(app.Config.PluginName, app.Directory)).ToList();
        }

        public override List<IPluginInfo> GetListOfInstalledDeviceDrivers(Current current__)
        {
            return _serviceHost.GetListOfInstalledDeviceDrivers().Select(deviceDriver => new IPluginInfo(deviceDriver.Config.PluginName, deviceDriver.Directory)).ToList();
        }

        public override void CloseApp(string fileName, Current current__)
        {
            _serviceHost.CloseApp(fileName);
        }

        public override void ActivateDeviceDrivers(Current current__)
        {
            _serviceHost.ActivateDrivers();
        }

        public override List<string> GetListOfEventTypes(Current current__)
        {
            return _serviceHost.GetListOfEventTypes().ToList();
        }

        public override void Register(string name, string category, int processId, Current current__)
        {
            _serviceHost.RegisterService(name, category, processId, current__);
        }

        public override void ReportEvent(string key, string value, Current current__)
        {
            _serviceHost.ReportEvent(new KeyValuePair<string, string>(key, value));
        }

        public override void SubscribeToEvents(string fullyQualifiedNameSpace, int processId, Current current__)
        {
            _serviceHost.SubscribeToEvents(fullyQualifiedNameSpace, processId);
        }

        public override void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId, Current current__)
        {
            _serviceHost.UnSubscribeToEvents(fullyQualifiedNameSpace, processId);
        }
    }
}
