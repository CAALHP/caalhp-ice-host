﻿using CAALHP.SOAICE.Contracts;
using Ice;
using System.Collections.Generic;

namespace CAALHP.SOA.ICE.HostAdapters
{
    public class DeviceDriverHostAdapter : IDeviceDriverHostContractDisp_
    {
        //private IHostContract _host;
        private readonly IDeviceDriverHostIce _deviceDriverHost;

        public DeviceDriverHostAdapter(IDeviceDriverHostIce host)
        {
            _deviceDriverHost = host;
            //_host = new HostAdapter(_deviceDriverHost);
        }

        public override void Register(string name, string category, int processId, Current current__)
        {
            _deviceDriverHost.RegisterDeviceDriver(name, category, processId, current__);
        }

        public override void ReportEvent(string key, string value, Current current__)
        {
            _deviceDriverHost.ReportEvent(new KeyValuePair<string, string>(key, value));
        }

        public override void SubscribeToEvents(string fullyQualifiedNameSpace, int processId, Current current__)
        {
            _deviceDriverHost.SubscribeToEvents(fullyQualifiedNameSpace, processId);
        }

        public override void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId, Current current__)
        {
            _deviceDriverHost.UnSubscribeToEvents(fullyQualifiedNameSpace, processId);
        }
    }
}
