﻿using CAALHP.Contracts;
using CAALHP.SOAICE.Contracts;
using Ice;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CAALHP.SOA.ICE.ClientAdapters
{
    public class AppAdapter : IAppContractDisp_
    {
        private readonly IAppCAALHPContract _appContract;
        private Communicator _ic;
        private Identity _ident;
        public IAppHostContractPrx HostProxy { get; set; }

        public AppAdapter(string endpoint, IAppCAALHPContract appContract)
        {
            _appContract = appContract;
            Connect(endpoint);
            var attachedSuccessfully = WaitForAttachment();
            if (!attachedSuccessfully)
            {
                throw new ConnectFailedException();
            }
        }

        /// <summary>
        /// The connect application provides CareStore Apps and Drivers the ability to connect to the CAALHP CareStoreNameService.
        /// Thus, it will register with the CAALPH through the CareStoreNameService, and thus allow CAALHP to communicate cross-process
        /// </summary>
        /// <param name="endpoint">the endpoint to call</param>
        private void Connect(string endpoint)
        {
            _ic = null;
            try
            {
                new Task(() =>
                {
                    // Create a communicator
                    _ic = Ice.Util.initialize();

                    if (endpoint == null || endpoint.Equals(""))
                        endpoint = "127.0.0.1";

                    // Create a proxy 
                    var obj = _ic.stringToProxy("AppHost:default -h " + endpoint + " -p 10003");

                    // Down-cast the proxy to a CareStoreNameService proxy
                    HostProxy = IAppHostContractPrxHelper.checkedCast(obj);

                    //Create a nameless adapter for holding the 
                    var adapter = _ic.createObjectAdapter("");

                    //Create an identity for us to track the callback obeject
                    _ident = new Ice.Identity { name = Guid.NewGuid().ToString(), category = "App" };

                    //bind the identity with the implementation and activate it
                    adapter.add(this, _ident);
                    adapter.activate();

                    //activate the adapter for the callback
                    HostProxy.ice_getConnection().setAdapter(adapter);

                    //register the callback with the NameService object
                    HostProxy.Register(_ident.name, _ident.category, System.Diagnostics.Process.GetCurrentProcess().Id);
                    Console.WriteLine("Registered Service");
                    var appHostToCAALHPContractAdapter = new AppHostToCAALHPContractAdapter(HostProxy);
                    Console.WriteLine("Created servicehostadapter");
                    _appContract.Initialize(appHostToCAALHPContractAdapter, System.Diagnostics.Process.GetCurrentProcess().Id);
                    Console.WriteLine("Initialized Service");
                    _ic.waitForShutdown();
                }).Start();

            }
            catch (Ice.Exception e)
            {
                Console.Error.WriteLine(e);
                //status = 1;
            }
            if (_ic == null) return;
            // Clean up
            try
            {
                _ic.destroy();
            }
            catch (System.Exception e)
            {
                Console.Error.WriteLine(e);
                //status = 1;
            }
            //Environment.Exit(status);
        }

        private bool WaitForAttachment()
        {
            var counter = 0;

            while (!ConnectionEstablished() && counter < 50)
            {
                counter++;
                if (counter % 10 == 1)
                    Console.WriteLine("Waiting for attachment to Service Host");
                Thread.Sleep(100);
            }

            return ConnectionEstablished();
        }

        private bool ConnectionEstablished()
        {
            return HostProxy != null && _ident != null;
        }
        

        public override void Show(Current current__)
        {
            _appContract.Show();
        }

        public override void Hide(Current current__)
        {
            //_app.Hide();
        }

        public override string GetName(Current current__)
        {
            return _appContract.GetName();
        }

        public override bool IsAlive(Current current__)
        {
            return _appContract.IsAlive();
        }

        public override void ShutDown(Current current__)
        {
            _appContract.ShutDown();
        }

        public override void Notify(string key, string value, Current current__)
        {
            _appContract.Notify(new KeyValuePair<string, string>(key, value));
        }
    }
}
