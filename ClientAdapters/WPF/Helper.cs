﻿using System.Windows;

namespace CAALHP.SOA.ICE.ClientAdapters.WPF
{
    public static class Helper
    {
        public static void BringToFront()
        {
            var window = Application.Current.MainWindow;
            if (!window.IsVisible)
            {
                window.Show();
            }
            window.WindowState = WindowState.Maximized;
            window.Activate();
            window.Topmost = true;
            window.Topmost = false;
            window.Focus();
        }
    }
}
