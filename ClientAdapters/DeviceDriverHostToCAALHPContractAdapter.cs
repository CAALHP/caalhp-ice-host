using CAALHP.Contracts;
using CAALHP.SOAICE.Contracts;
using System;

namespace CAALHP.SOA.ICE.ClientAdapters
{
    public class DeviceDriverHostToCAALHPContractAdapter : IDeviceDriverHostCAALHPContract
    {
        private IDeviceDriverHostContractPrx _deviceDriverHostContract;

        public DeviceDriverHostToCAALHPContractAdapter(IDeviceDriverHostContractPrx host)
        {
            Console.WriteLine("Constructing DeviceDriverHostToCAALHPContractAdapter");
            _deviceDriverHostContract = host;
            try
            {
                Console.WriteLine("_deviceDriverHostContract.GetHost");
                Host = new HostToCAALHPContractAdapter(_deviceDriverHostContract);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public IHostCAALHPContract Host { get; set; }
    }
}