﻿using CAALHP.Contracts;
using CAALHP.SOAICE.Contracts;
using Ice;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CAALHP.SOA.ICE.ClientAdapters
{
    public class DeviceDriverAdapter : IDeviceDriverContractDisp_
    {
        private Ice.Communicator _ic;
        private Ice.Identity _ident;
        private readonly IDeviceDriverCAALHPContract _deviceDriverContract;
        public IDeviceDriverHostContractPrx HostProxy = null;
        
        public DeviceDriverAdapter(string endpoint, IDeviceDriverCAALHPContract host)
        {
            Console.WriteLine("Constructing DeviceDriverAdapter");
            _deviceDriverContract = host;
            Connect(endpoint);
            var attachedSuccessfully = WaitForAttachment();
            if (!attachedSuccessfully)
            {
                throw new ConnectFailedException();
            }
        }

        private void Connect(string endpoint)
        {
            _ic = null;
            try
            {
                new Task(() =>
                {
                    // Create a communicator
                    _ic = Ice.Util.initialize();

                    if (endpoint == null || endpoint.Equals(""))
                        endpoint = "127.0.0.1";

                    // Create a proxy 
                    var obj = _ic.stringToProxy("DeviceDriverHost:default -h " + endpoint + " -p 10002");

                    // Down-cast the proxy to a CareStoreNameService proxy
                    HostProxy = IDeviceDriverHostContractPrxHelper.checkedCast(obj);

                    //Create a nameless adapter for holding the 
                    var adapter = _ic.createObjectAdapter("");

                    //Create an identity for us to track the callback object
                    _ident = new Ice.Identity { name = Guid.NewGuid().ToString(), category = "DeviceDriver" };

                    //bind the identity with the implementation and activate it
                    adapter.add(this, _ident);
                    adapter.activate();

                    //activate the adapter for the callback
                    HostProxy.ice_getConnection().setAdapter(adapter);

                    //register the callback with the NameService object
                    HostProxy.Register(_ident.name, _ident.category, System.Diagnostics.Process.GetCurrentProcess().Id);
                    Console.WriteLine("Registered DeviceDriver");
                    var deviceDriverHostToCAALHPContractAdapter = new DeviceDriverHostToCAALHPContractAdapter(HostProxy);
                    Console.WriteLine("Created devicedriverhostadapter");
                    _deviceDriverContract.Initialize(deviceDriverHostToCAALHPContractAdapter, System.Diagnostics.Process.GetCurrentProcess().Id);
                    Console.WriteLine("Initialized DeviceDriver");
                    _ic.waitForShutdown();
                }).Start();

            }
            catch (Ice.Exception e)
            {
                Console.Error.WriteLine(e);
                //status = 1;
            }
            if (_ic == null) return;
            // Clean up
            try
            {
                _ic.destroy();
            }
            catch (System.Exception e)
            {
                Console.Error.WriteLine(e);
                //status = 1;
            }
        }

        private bool WaitForAttachment()
        {
            var counter = 0;

            while (!ConnectionEstablished() && counter < 50)
            {
                counter++;
                if(counter%10 == 1)
                    Console.WriteLine("Waiting for attachment to DeviceDriver Host");
                Thread.Sleep(100);
            }

            return ConnectionEstablished();
        }

        private bool ConnectionEstablished()
        {
            return HostProxy != null && _ident != null;
        }

        public override string GetName(Current current__)
        {
            return _deviceDriverContract.GetName();
        }

        public override bool IsAlive(Current current__)
        {
            return _deviceDriverContract.IsAlive();
        }

        public override void ShutDown(Current current__)
        {
            _deviceDriverContract.ShutDown();
        }

        public override void Notify(string key, string value, Current current__)
        {
            _deviceDriverContract.Notify(new KeyValuePair<string, string>(key, value));
        }
    }
}
