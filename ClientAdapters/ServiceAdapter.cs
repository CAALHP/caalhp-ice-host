﻿using CAALHP.Contracts;
using CAALHP.SOAICE.Contracts;
using Ice;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CAALHP.SOA.ICE.ClientAdapters
{
    public class ServiceAdapter : IServiceContractDisp_
    {
        private Ice.Communicator _ic;
        private Ice.Identity _ident;
        public IServiceHostContractPrx HostProxy = null;
        private readonly IServiceCAALHPContract _serviceContract;
        
        public ServiceAdapter(string endpoint, IServiceCAALHPContract serviceContract)
        {
            _serviceContract = serviceContract;
            Connect(endpoint);
            var attachedSuccessfully = WaitForAttachment();
            if (!attachedSuccessfully)
            {
                throw new ConnectFailedException();
            }
        }

        public override void Start(Current current__)
        {
            _serviceContract.Start();
        }

        public override void Stop(Current current__)
        {
            _serviceContract.Stop();
        }

        /*public override void Initialize(IServiceHostContract host, int processId, Current current__)
        {
            //Don't forward these messages to the client, as they should be initialized during connection.
            //var hostAdapter = new ServiceHostToCAALHPContractAdapter(host);
            //_serviceContract.Initialize(null, processId);
        }*/

        public override string GetName(Current current__)
        {
            return _serviceContract.GetName();
        }

        public override bool IsAlive(Current current__)
        {
            return _serviceContract.IsAlive();
        }

        public override void ShutDown(Current current__)
        {
            _serviceContract.ShutDown();
        }

        public override void Notify(string key, string value, Current current__)
        {
            _serviceContract.Notify(new KeyValuePair<string, string>(key, value));
        }


        /// <summary>
        /// The connect application provides CareStore Apps and Drivers the ability to connect to the CAALHP CareStoreNameService.
        /// Thus, it will register with the CAALPH through the CareStoreNameService, and thus allow CAALHP to communicate cross-process
        /// </summary>
        /// <param name="endpoint">the endpoint to call</param>
        private void Connect(string endpoint)
        {
            _ic = null;
            try
            {
                new Task(() =>
                {
                    // Create a communicator
                    _ic = Ice.Util.initialize();

                    if (endpoint == null || endpoint.Equals(""))
                        endpoint = "127.0.0.1";

                    // Create a proxy 
                    var obj = _ic.stringToProxy("ServiceHost:default -h " + endpoint + " -p 10001");
                    
                    // Down-cast the proxy to a CareStoreNameService proxy
                    HostProxy = IServiceHostContractPrxHelper.checkedCast(obj);

                    //Create a nameless adapter for holding the 
                    var adapter = _ic.createObjectAdapter("");

                    //Create an identity for us to track the callback obeject
                    _ident = new Ice.Identity { name = Guid.NewGuid().ToString(), category = "Service" };

                    //bind the identity with the implementation and activate it
                    adapter.add(this, _ident);
                    adapter.activate();

                    //activate the adapter for the callback
                    HostProxy.ice_getConnection().setAdapter(adapter);
                    
                    //register the callback with the NameService object
                    HostProxy.Register(_ident.name, _ident.category, System.Diagnostics.Process.GetCurrentProcess().Id);
                    Console.WriteLine("Registered Service");
                    var serviceHostToCAALHPContractAdapter = new ServiceHostToCAALHPContractAdapter(HostProxy);
                    Console.WriteLine("Created servicehostadapter");
                    _serviceContract.Initialize(serviceHostToCAALHPContractAdapter, System.Diagnostics.Process.GetCurrentProcess().Id);
                    Console.WriteLine("Initialized Service");
                    _ic.waitForShutdown();
                }).Start();

            }
            catch (Ice.Exception e)
            {
                Console.Error.WriteLine(e);
                //status = 1;
            }
            if (_ic == null) return;
            // Clean up
            try
            {
                _ic.destroy();
            }
            catch (System.Exception e)
            {
                Console.Error.WriteLine(e);
                //status = 1;
            }
            //Environment.Exit(status);
        }

        private bool WaitForAttachment()
        {
            var counter = 0;

            while (!ConnectionEstablished() && counter < 50)
            {
                counter++;
                if(counter%10 == 1)
                    Console.WriteLine("Waiting for attachment to Service Host");
                Thread.Sleep(100);
            }

            return ConnectionEstablished();
        }

        private bool ConnectionEstablished()
        {
            return HostProxy != null && _ident != null;
        }
    }
}
