using CAALHP.Contracts;
using CAALHP.SOAICE.Contracts;
using System;
using System.Collections.Generic;

namespace CAALHP.SOA.ICE.ClientAdapters
{
    public class HostToCAALHPContractAdapter : IHostCAALHPContract
    {
        private readonly IHostContractPrx _hostContract;

        public HostToCAALHPContractAdapter(IHostContractPrx host)
        {
            Console.WriteLine("Constructing HostToCAALHPContractAdapter");
            _hostContract = host;

        }

        public void ReportEvent(KeyValuePair<string, string> value)
        {
            _hostContract.ReportEvent(value.Key, value.Value);
        }
        
        public void SubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            _hostContract.SubscribeToEvents(fullyQualifiedNameSpace, processId);
        }
        
        public void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            _hostContract.UnSubscribeToEvents(fullyQualifiedNameSpace, processId);
        }
    }
}