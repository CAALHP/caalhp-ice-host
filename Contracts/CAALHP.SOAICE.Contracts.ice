#pragma once

#include <Ice/Identity.ice>

module CAALHP{ 
	module SOAICE{
			module Contracts{
				interface IHostContract { 
					void Register(string name, string category, int processId);
					void ReportEvent(string key, string value);
					void SubscribeToEvents(string fullyQualifiedNameSpace, int processId);
					void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId);
					//void Show(string AppNameToShow);
				}; 

				["clr:property", "clr:class"] 
				struct IPluginInfo
				{
					string Name;
					string LocationDir;
				};

				["clr:generic:List"]
				sequence<IPluginInfo> IPluginInfoSeq;
				
				["clr:generic:List"]
				sequence<string> StringSeq;

				interface IServiceHostContract extends IHostContract
				{
					IPluginInfoSeq GetListOfInstalledApps();
					IPluginInfoSeq GetListOfInstalledDeviceDrivers();
					void CloseApp(string fileName);
					void ActivateDeviceDrivers();
					StringSeq GetListOfEventTypes();
				};

				interface IDeviceDriverHostContract extends IHostContract
				{
					
				};

				interface IAppHostContract extends IHostContract
				{
					void ShowApp(string appName);
					void CloseApp(string appName);
					IPluginInfoSeq GetListOfInstalledApps();
				};

				interface IPluginBaseContract { 
					string GetName(); 
					bool IsAlive();
					void ShutDown();
					void Notify(string key, string value);
				}; 

				interface IAppContract extends IPluginBaseContract
				{ 
					void Show();
					void Hide();
				}; 

				interface IDeviceDriverContract extends IPluginBaseContract 
				{
					//double GetMeasurement();
				};

				interface IServiceContract extends IPluginBaseContract
				{
					void Start();
					void Stop();
					//void Initialize(IServiceHostContract host, int processId);
				};
			};
	};
}; 